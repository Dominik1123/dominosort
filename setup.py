from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(
    name='dominosort',
    version='0.1',
    description='Sort sequences with respect to the similarity of consecutive items.',
    long_description=readme(),
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering',
        'Topic :: Utilities',
    ],
    keywords=['sort', 'sorting', 'algorithm'],
    url='https://gitlab.com/Dominik1123/dominosort',
    author='Dominik Vilsmeier',
    license='BSD-3-Clause',
    packages=['dominosort'],
    install_requires=['numpy>=1.4.0', 'scipy>=0.15.0'],
    include_package_data=True,
    zip_safe=False
)
